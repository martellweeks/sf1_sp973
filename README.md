# SF1_sp973

This is a repository for MATLAB code written during Part IIA Project, SF1: Data Analysis, Cambridge University Engineering Department.
While most code implementation and signal processing results are my own work, the total structure of the project as well as the content sound files is provided by Cambridge University Engineering Department.
This repository is for reference purposes: I do not own the rights to the content of SF1 project.

## Overview

The repository contains codes and audio files implemented for signal processing tasks during SF1: Data analysis project.
The MATLAB codes for each week's tasks are contained in folders separate for each week.

 - `Week 1`: DFT, FFT, Frequency spectrum analysis of music and speech signals
 - `Week 2`: Noise reduction filter implementation, using Wiener filter and power/amplitude subtraction filter
 - `Week 3 and 4`: Maximum Likelihood estimate and Bayesian inference, missing packet concealment using Bayesian inference

Processed audio signals are also in separate folders: `Audio_1` for Week 1, `noisy_audio` for Week 2, `missing_packet` for Week 3 and 4.

