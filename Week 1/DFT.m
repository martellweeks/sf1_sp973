% Code for computing DFT of a discrete time signal

function dft_spectrum = DFT(signal)
    N = length(signal);
    ii = 0:N-1;
    jj = transpose(0:N-1);
    G = exp(-2j*pi.*ii.*jj/N);
    dft_spectrum = signal * G;
end