% Plots windowed spectrum analysis of oscillating functions with specific
% amplitude modulation, and plays sound of the signal
% Parameters - func_type: exponential, sin or cos wave. freq: resonating
% freuquency of signal. frame: length of window. window_type: hamming, hanning,
% rectangular. noise: level of random Gaussian noise. modulation: linear
% gain, periodic modulation, stochastic random evolution.

function spectrum = ampmod(func_type, freq, frame, window_type, noise, modulation)
    N = 0:frame-1;
    DFTlen = 1024; % Length of DFT performed
    if ~exist('noise','var')
        noise = 0;
    end
    switch window_type
        case 1 % Hamming
            window = transpose(hamming(frame));
        case 2 % Hanning
            window = transpose(hann(frame));
        case 3 % Rectangular
            window = ones([1, frame]);
    end

    switch func_type
        case 1 % exp(jwn)
            signal = window.*(exp(1j*freq.*N)+noise*randn([1,frame]));
        case 2 % sin(wn)
            signal = window.*(sin(freq.*N)+noise*randn([1,frame]));
        case 3 % cos(wn)
            signal = window.*(cos(freq.*N)+noise*randn([1,frame]));
    end

    switch modulation
        case 1 % linear gain
            signal1 = signal + (0.005.*N+1);
            signal2 = signal + (0.05.*N+1);
        case 2 % periodic modulation
            signal1 = (1+0.05*sin((pi/20).*N)).*signal;
            signal2 = (1+0.05*sin((pi/10).*N)).*signal;
        case 3 % random stochastic evolution
            gain = ones([1,frame]);
            gain(1) = randn;
            for i=2:frame
                gain(i)=gain(i-1)+randn;
            end
            signal1 = gain.*signal;
            gain = ones([1,frame]);
            gain(1) = 10*randn;
            for i=2:frame
                gain(i)=gain(i-1)+10*randn;
            end
            signal2 = gain.*signal;
        otherwise
            signal = signal;
    end
    
    % Play sound of signal
    
    % sound(signal);

    % Plot frequency spectrum via FFT along with wvtool analyser

    freqaxis = linspace(0, 2*pi, DFTlen);
    freqdata1 = fft(signal1, DFTlen);
    freqdata2 = fft(signal2, DFTlen);
    plot(freqaxis(1:DFTlen/2), db(abs(freqdata1(1:DFTlen/2))), freqaxis(1:DFTlen/2), db(abs(freqdata2(1:DFTlen/2))));
    xlim([0 pi])
    title('Frequency spectrum, sin(wn), random noise gain')
    xlabel('normalised freq, rad/s')
    ylabel('magnitude, dB')
    legend('var=1', 'var=sqrt(10)')
    %wvtool(signal)
end