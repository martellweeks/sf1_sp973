% Glock: B5, Transient: 9000-11000, SS: 16000-18000, Final: 100000-105000


function spectrum = music()
    [signal, Fs] = audioread('Audio_1\organ.wav');
    start = 21000;
    N = 2048;
    disp(Fs)
    disp(length(signal))
    DFTlen = 9192;
    section = signal(start: start+N-1,1);
    window = hamming(N);
    freqaxis = linspace(0, Fs, DFTlen);
    freqdata1 = fft(window.*section, DFTlen);
    %start = 16000;
    %section = signal(start: start+N-1,1);
    sound(section, Fs)
    %freqdata2 = fft(window.*section, DFTlen);
    plot(freqaxis(1:DFTlen/2), db(abs(freqdata1(1:DFTlen/2))));
    %plot(freqaxis(1:DFTlen/2), db(abs(freqdata1(1:DFTlen/2))),freqaxis(1:DFTlen/2), db(abs(freqdata2(1:DFTlen/2))));
    xlim([0 1000])
    title('Frequency spectrum of organ')
    %legend('Transient','Steady-state')
    xlabel('Frequency, Hz')
    ylabel('magnitude, dB')
end