% Plots windowed spectrum analysis of oscillating functions
% Parameters - func_type: exponential, sin or cos wave. freq: resonating
% freuquency of signal. frame: length of window. window_type: hamming, hanning,
% rectangular. noise: level of random Gaussian noise.

function spectrum = oscillate(func_type, freq, frame, window_type, noise)
    if ~exist('noise','var')
        noise = 0;
    end
    N = 0:frame-1;
    L = 300; % DFT length

    switch window_type
        case 1 % Hamming
            window = transpose(hamming(frame));
        case 2 % Hanning
            window = transpose(hann(frame));
        case 3 % Rectangular
            window = ones([1, frame]);
    end

    switch func_type
        case 1 % exp(jwn)
            signal = window.*(exp(1j*freq.*N)+noise*randn([1,frame]));
        case 2 % sin(wn)
            signal = window.*(sin(freq.*N)+noise*randn([1,frame]));
        case 3 % cos(wn)
            signal = window.*(cos(freq.*N)+noise*randn([1,frame]));
    end

    % Plot frequency spectrum via FFT
    freqaxis = linspace(0, 2*pi, L);
    freqdata = fft(signal, L);
    plot(freqaxis(1:L/2), db(abs(freqdata(1:L/2))));
    xlim([0 pi])
    title('Frequency spectrum, sin(wn), hamming')
    xlabel('normalised freq, rad/s')
    ylabel('magnitude, dB')
end