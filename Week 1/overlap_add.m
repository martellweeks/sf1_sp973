% Overlap-add method for applying a bandpass filter

% Input signal, cos(n*pi/4) + sin(n*pi/100) + Gaussian noise
x_in=0.5*cos([1:10000]*pi/4)+sin([1:10000]*pi/100)+randn(1,10000);
y_out=0*x_in; % Output signal

N=512; % DFT block length = sub-frame length
overlap=256; % Overlap = N/2

x=buffer(x_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(x);
x_w=repmat(hanning(N),1,N_frames).*x; % Apply Hanning window to sub-frames

for frame_no=1:N_frames-2
    X_w(:,frame_no)=fft(x_w(:,frame_no));
    Y_w(:,frame_no)=X_w(:,frame_no);
    Y_w(2:N/8,frame_no)=0.1*X_w(2:N/8,frame_no); % Attenuate low quartile freq to 0.1x
    Y_w(N/4+1:N/2,frame_no)=0.2*X_w(N/4+1:N/2,frame_no); % Attenuate higher half freq to 0.2x
    Y_w(N:-1:N/2+2,frame_no)=conj(Y_w(2:N/2,frame_no)); % Conjugate symmetry
    y_w(:,frame_no)=ifft(Y_w(:,frame_no));
    % Now overlap-add method
    y_out((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
    y_out((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+transpose(y_w(:,frame_no));
end

timeaxis = linspace(0, 10000, 10000);
freqaxis = linspace(0, 2*pi, N);
y_w=buffer(y_out,N,overlap);
X_spect = db(abs(fft(x_w(:,32))));
Y_spect = db(abs(fft(y_w(:,32))));

plot(freqaxis(1:N/2), X_spect(1:N/2), freqaxis(1:N/2), Y_spect(1:N/2))
title('Bandpass filter with low cutoff pi/4, high cutoff pi/2')
legend('Original signal','Filtered signal')
xlim([0,pi])
xlabel('Normalised frequency, rad/s')
ylabel('magnitude, dB')
