% Plots time complexity graph of straight N-DFT and FFT

function res = plotComplexity()
    ns_DFT = linspace(1000, 100000, 10);
    ns_FFT = linspace(100, 10000, 10);
    DFTtime = ones([1,10]);
    FFTtime = ones([1,10]);
    for i = 1:10
        disp(i)
        testdata = rand(1, ns_DFT(i));
        DFTtime(i) = time_DFT(testdata);
    end
    for i = 1:10
        disp(i)
        testdata_2 = rand(1, ns_FFT(i));
        FFTtime(i) = time_FFT(testdata_2);
    end

    figure
    tiledlayout(2,1)
    
    ax1 = nexttile;
    plot(ns_DFT, DFTtime);
    title('Time complexity of straight N-DFT')
    xlabel('N')
    ylabel('time/s')

    ax2 = nexttile;
    plot(ns_FFT, FFTtime);
    title('Time complexity of FFT')
    xlabel('N')
    ylabel('time/s')
end


% Measures time taken to perform straight N-DFT

function result = time_DFT(signal)
    tic
    DFT(signal);
    result = toc;
end


% Measures time taken to perform FFT

function result = time_FFT(signal)
    tic
    fft(signal);
    result = toc;
end