% Speech SO. (m2rdisum) Consonant: 19000, Vowel: 22000, N=1024

% Speech JOHN. (f1lcapae) Consonant: 44000, Vowel: 45500, N=512, 4096-DFT

function spectrum = speech()
    [signal, Fs] = audioread('Audio_1\f1lcapae.wav');
    start = 44000;
    N = 512;
    disp(Fs)
    disp(length(signal))
    DFTlen = 512;
    section = signal(start: start+N-1,1);
    %sound(section, Fs)
    window = hamming(N);
    freqaxis = linspace(0, Fs, DFTlen);
    freqdata1 = fft(window.*section, DFTlen);
    start = 45500;
    section = signal(start: start+N-1,1);
    sound(section, Fs)
    freqdata2 = fft(window.*section, DFTlen);
    %plot(freqaxis(1:DFTlen/2), db(abs(freqdata1(1:DFTlen/2))));
    plot(freqaxis(1:DFTlen/2), db(abs(freqdata1(1:DFTlen/2))),freqaxis(1:DFTlen/2), db(abs(freqdata2(1:DFTlen/2))));
    xlim([0 freqaxis(DFTlen/2)])
    title('Frequency spectrum of speech - word JOHN')
    legend('Consonant J','Vowel O')
    xlabel('Frequency, Hz')
    ylabel('magnitude, dB')
end