% Overlap-add method to apply Power subtraction filter for noise reduction

clear

% Reference signal if exists
%[sig_ref, Fs_ref] = audioread('noisy_audio\female_speech_noisy_col.wav');

% Noisy input signal
[sig, Fs] = audioread('noisy_audio\male_speech_noisy_soft.wav');
y_in = sig(:,1); % Convert stereo signal to mono (by just simply taking left)
x_hat=0*y_in; % Estimated original signal

N=1024; % DFT block length = sub-frame length
overlap=N/2; % Overlap = N/2
ngain=6; % Gain of noise PSD applied to filter

y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(y);
y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames

% Estimation of noise psd
noise = y_in(1:Fs/2); % First 0.5 sec
noise_b = buffer(noise,N,overlap);
[N_samps_n,N_frames_n]=size(noise_b);
noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
noise_psd = zeros(1,N);
for frame_no=1:N_frames_n-2
    N_w(:,frame_no)=fft(noise_w(:,frame_no));
    noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
end
noise_psd = ngain*noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise
disp(N_frames_n)

for frame_no=1:N_frames-2
    Y_w(:,frame_no)=fft(y_w(:,frame_no));
    X_w(:,frame_no)=Y_w(:,frame_no);
    for i=2:length(Y_w((1:N/2),frame_no))
        if abs(Y_w(i,frame_no))^2 > noise_psd(i)
            X_w(i,frame_no) = Y_w(i,frame_no)*sqrt(((abs(Y_w(i,frame_no))^2)-noise_psd(i))/(abs(Y_w(i,frame_no))^2));
        else
            X_w(i,frame_no) = 0;
        end
    end
    X_w(N:-1:N/2+2,frame_no)=conj(X_w(2:N/2,frame_no)); % Conjugate symmetry
    x_hat_w(:,frame_no)=ifft(X_w(:,frame_no));
    % Now overlap-add method
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+x_hat_w(:,frame_no);
end

% Calculate MSE
%mse = mean((x_hat-sig_ref).^2);
%fprintf(mse)

% Plots
timeaxis = linspace(0, length(x_hat)/Fs, length(x_hat));
freqaxis = linspace(0, 2*pi, N);
x_hat_frame=buffer(x_hat,N,overlap);
x_hat_frame_w=repmat(hamming(N),1,N_frames).*x_hat_frame;

tiledlayout(2,1)
nexttile % Show noise PSD
plot(freqaxis, abs(fft(noise_psd)))
nexttile % Show a spectrum of sub-frame before and after filter
plot(freqaxis, (abs(fft(x_hat_frame_w(:,100)))), freqaxis, (abs(fft(y_w(:,100)))))

% Play sound of extracts
sound(y_in(1:round(length(y_in)/3)),Fs)
pause(5)
sound(x_hat(1:round(length(x_hat)/3)),Fs)

% Save file if needed
% audiowrite("noisy_audio\filtered\male_speech_noisy_soft_n1024_g6.wav",x_hat,Fs)