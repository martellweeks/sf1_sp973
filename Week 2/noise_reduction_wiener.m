% Overlap-add method to apply Wiener filter for noise reduction

clear

% Reference signal if exists
%[sig_ref, Fs_ref] = audioread('noisy_audio\female_speech_noisy_col.wav');

% Noisy input signal
[sig, Fs] = audioread('noisy_audio\filtered\female_speech_noisy_loud_double.wav');
y_in = sig(:,1); % Convert stereo signal to mono (by just simply taking left)
x_hat=0*y_in; % Estimated original signal

N=1024; % DFT block length = sub-frame length
overlap=N/2; % Overlap = N/2
ngain=15; % Gain of noise PSD applied to filter

y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(y);
y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames

% Estimation of noise psd
noise = y_in(1:Fs/2); % First 0.5 sec
noise_b = buffer(noise,N,overlap);
[N_samps_n,N_frames_n]=size(noise_b);
noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
noise_psd = zeros(1,N);
for frame_no=1:N_frames_n-2
    N_w(:,frame_no)=fft(noise_w(:,frame_no));
    noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
end
noise_psd = noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise
noise_psd_amplified = ngain*noise_psd;

for frame_no=1:N_frames-2
    Y_w(:,frame_no)=fft(y_w(:,frame_no));
    X_w(:,frame_no)=Y_w(:,frame_no);
    for i=2:length(Y_w((1:N/2),frame_no))
        if abs(Y_w(i,frame_no))^2 > noise_psd_amplified(i)
            X_w(i,frame_no) = Y_w(i,frame_no)*((abs(Y_w(i,frame_no))^2)-noise_psd_amplified(i))/(abs(Y_w(i,frame_no))^2);
        else
            X_w(i,frame_no) = 0;
        end
    end
    X_w(N:-1:N/2+2,frame_no)=conj(X_w(2:N/2,frame_no)); % Conjugate symmetry
    x_hat_w(:,frame_no)=ifft(X_w(:,frame_no));
    % Now overlap-add method
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+x_hat_w(:,frame_no);
end


% Plots
timeaxis = linspace(0, length(x_hat)/Fs, length(x_hat));
freqaxis = linspace(0, 2*pi, N);
x_hat_frame=buffer(x_hat,N,overlap);
x_hat_frame_w=repmat(hamming(N),1,N_frames).*x_hat_frame;

tiledlayout(2,1)
nexttile % Show noise PSD
plot(freqaxis, db(abs(fft(noise_psd))))
title("Estimated power spectrum of noise")
xlabel("Normalised freq, rad/s")
ylabel("gain")
xlim([0 pi/2])
nexttile % Show a spectrum of sub-frame before and after filter
plot(freqaxis, db(abs(fft(x_hat_frame_w(:,20)))), freqaxis, db(abs(fft(y_w(:,20)))))
title("Effect of noise reduction on frequency spectrum of signal")
xlabel("Normalised freq, rad/s")
ylabel("gain")
xlim([0 pi/2])

% Play sound of extracts
sound(y_in(1:round(length(y_in)/2)),Fs)
pause(5)
sound(x_hat(1:round(length(x_hat)/2)),Fs)

% Save file if needed
%audiowrite("noisy_audio\filtered\female_speech_noisy_loud_triple.wav",x_hat,Fs)