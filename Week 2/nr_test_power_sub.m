% Overlap-add method to apply noise then reduce noise using Power subtraction filter

clear

% Clean input signal
[sig, Fs] = audioread('Audio_1\m2rdisum.wav');
x_in = sig(:,1); % Convert stereo signal to mono (by just simply taking left)
x_in = x_in(100000:150000);
%sound(x_in,Fs);

% Add noise to input signal
add_noise = 0.01*randn(length(x_in),1);
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal

N=1024; % DFT block length = sub-frame length
overlap=N/2; % Overlap = N/2
ngain=1; % Gain of noise PSD applied to filter

y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(y);
y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames

% Estimation of noise psd
noise = y_in(1:Fs/2); % First 0.5 sec
noise_b = buffer(add_noise,N,overlap);
[N_samps_n,N_frames_n]=size(noise_b);
noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
noise_psd = zeros(1,N);
for frame_no=1:N_frames_n-2
    N_w(:,frame_no)=fft(noise_w(:,frame_no));
    noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
end
noise_psd = ngain*noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise
disp(N_frames_n)

for frame_no=1:N_frames-2
    Y_w(:,frame_no)=fft(y_w(:,frame_no));
    X_w(:,frame_no)=Y_w(:,frame_no);
    for i=2:length(Y_w((1:N/2),frame_no))
        if abs(Y_w(i,frame_no))^2 > noise_psd(i)
            X_w(i,frame_no) = Y_w(i,frame_no)*sqrt(((abs(Y_w(i,frame_no))^2)-noise_psd(i))/(abs(Y_w(i,frame_no))^2));
        else
            X_w(i,frame_no) = 0;
        end
    end
    X_w(N:-1:N/2+2,frame_no)=conj(X_w(2:N/2,frame_no)); % Conjugate symmetry
    x_hat_w(:,frame_no)=ifft(X_w(:,frame_no));
    % Now overlap-add method
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
    x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+x_hat_w(:,frame_no);
end

% Calculate MSE
mse = mean((abs(fft(x_hat))-abs(fft(x_in))).^2);
disp(mse)

% Plots
timeaxis = linspace(0, length(x_hat)/Fs, length(x_hat));
freqaxis = linspace(0, 2*pi, N);
x_hat_frame=buffer(x_hat,N,overlap);
x_hat_frame_w=repmat(hamming(N),1,N_frames).*x_hat_frame;

tiledlayout(2,1)
nexttile % Show noise PSD
plot(freqaxis, noise_psd);
xlim([0 pi]);
title("Power spectrum of additive Gaussian noise");
xlabel("normalised freq, rad/s");
ylabel("gain")
nexttile % Show a spectrum of sub-frame before and after filter
plot(freqaxis, (abs(fft(y_w(:,10)))), freqaxis, (abs(fft(x_hat_frame_w(:,10)))));
xlim([0 pi]);
title("Effect of noise reduction on spectrum of fragamented input signal");
xlabel("normalised freq, rad/s");
ylabel("gain")
legend("Noisy input signal", "Noise-reduced signal");

% Play sound of extracts
sound(y_in(1:round(length(y_in))),Fs)
pause(5)
sound(x_hat(1:round(length(x_hat))),Fs)

% Save file if needed
% audiowrite("noisy_audio\filtered\male_speech_noisy_soft_n1024_g6.wav",x_hat,Fs)