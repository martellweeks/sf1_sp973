% Overlap-add method to apply noise then reduce noise using Spectral subtraction filter

clear

% Clean input signal
[sig, Fs] = audioread('Audio_1\m2rdisum.wav');
x_in = sig(:,1); % Convert stereo signal to mono (by just simply taking left)
x_in = x_in(100000:150000);
%sound(x_in,Fs);

blocklen_list = [256, 512, 1024, 2048, 4096];
mse_1 = zeros(5,1);
mse_2 = zeros(5,1);
mse_3 = zeros(5,1);


% Add noise to input signal
add_noise = randn(length(x_in),1).*0.001;
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal
X_IN = abs(fft(x_in));

for g=1:5
    x_hat_g = wiener(y_in, Fs, add_noise, blocklen_list(g), x_hat, blocklen_list(g)/2);
    mse_1(g)=mean((abs(fft(x_hat_g))-X_IN).^2);
end

add_noise = randn(length(x_in),1).*0.01;
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal

for g=1:5
    x_hat_g = wiener(y_in, Fs, add_noise, blocklen_list(g), x_hat, blocklen_list(g)/2);
    mse_2(g)=mean((abs(fft(x_hat_g))-abs(fft(x_in))).^2);
end

add_noise = randn(length(x_in),1).*0.1;
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal

for g=1:5
    x_hat_g = wiener(y_in, Fs, add_noise, blocklen_list(g), x_hat, blocklen_list(g)/2);
    mse_3(g)=mean((abs(fft(x_hat_g))-abs(fft(x_in))).^2);
end

% Plots
tiledlayout(3,1)
nexttile
plot(blocklen_list,mse_1)
title("Plot of MSE over DFT length, noise var = 0.001")
xlabel("DFT length")
ylabel("MSE")
nexttile
plot(blocklen_list,mse_2)
title("Plot of MSE over DFT length, noise var = 0.01")
xlabel("DFT length")
ylabel("MSE")
nexttile
plot(blocklen_list,mse_3)
title("Plot of MSE over DFT length, noise var = 0.1")
xlabel("DFT length")
ylabel("MSE")

function x_hat = wiener(y_in, Fs, add_noise, N, x_hat, overlap)
    y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
    [N_samps,N_frames]=size(y);
    y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames
    
    % Estimation of noise psd
    noise = y_in(1:Fs/2); % First 0.5 sec
    noise_b = buffer(add_noise,N,overlap);
    [N_samps_n,N_frames_n]=size(noise_b);
    noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
    noise_psd = zeros(1,N);
    for frame_no=1:N_frames_n-2
        N_w(:,frame_no)=fft(noise_w(:,frame_no));
        noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
    end
    noise_psd = noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise
    
    for frame_no=1:N_frames-2
        Y_w(:,frame_no)=fft(y_w(:,frame_no));
        X_w(:,frame_no)=Y_w(:,frame_no);
        for i=2:length(Y_w((1:N/2),frame_no))
            if abs(Y_w(i,frame_no))^2 > noise_psd(i)
                X_w(i,frame_no) = Y_w(i,frame_no)*((abs(Y_w(i,frame_no))^2)-noise_psd(i))/(abs(Y_w(i,frame_no))^2);
            else
                X_w(i,frame_no) = 0;
            end
        end
        X_w(N:-1:N/2+2,frame_no)=conj(X_w(2:N/2,frame_no)); % Conjugate symmetry
        x_hat_w(:,frame_no)=ifft(X_w(:,frame_no));
        % Now overlap-add method
        x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
        x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+x_hat_w(:,frame_no);
    end
end