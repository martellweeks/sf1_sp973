% Overlap-add method to apply noise then reduce noise using Spectral subtraction filter

clear

% Clean input signal
[sig, Fs] = audioread('Audio_1\m2rdisum.wav');
x_in = sig(:,1); % Convert stereo signal to mono (by just simply taking left)
x_in = x_in(100000:150000);
%sound(x_in,Fs);

gain_list = linspace(0.5,50,100);
mse_1 = zeros(100,1);
mse_2 = zeros(100,1);


% Add noise to input signal
add_noise = 0.001*randn(length(x_in),1);
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal

N=1024; % DFT block length = sub-frame length
overlap=N/2; % Overlap = N/2
ngain=1; % Gain of noise PSD applied to filter

y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(y);
y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames

% Estimation of noise psd
noise = y_in(1:Fs/2); % First 0.5 sec
noise_b = buffer(add_noise,N,overlap);
[N_samps_n,N_frames_n]=size(noise_b);
noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
noise_psd = zeros(1,N);
for frame_no=1:N_frames_n-2
    N_w(:,frame_no)=fft(noise_w(:,frame_no));
    noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
end
noise_psd = noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise

for g=1:100
    x_hat_g = wiener(noise_psd.*(g/2), N_frames, y_w, N, x_hat, overlap);
    mse_1(g)=mean((abs(fft(x_hat_g))-abs(fft(x_in))).^2);
end

% Add noise to input signal
add_noise = 0.005*randn(length(x_in),1);
y_in = x_in + add_noise; % Noisy input signal
x_hat=0*y_in; % Estimated original signal

N=1024; % DFT block length = sub-frame length
overlap=N/2; % Overlap = N/2
ngain=1; % Gain of noise PSD applied to filter

y=buffer(y_in,N,overlap); % Divide into overlapping sub-frames
[N_samps,N_frames]=size(y);
y_w=repmat(hamming(N),1,N_frames).*y; % Apply Hamming window to sub-frames

% Estimation of noise psd
noise = y_in(1:Fs/2); % First 0.5 sec
noise_b = buffer(add_noise,N,overlap);
[N_samps_n,N_frames_n]=size(noise_b);
noise_w=repmat(hamming(N),1,N_frames_n).*noise_b;
noise_psd = zeros(1,N);
for frame_no=1:N_frames_n-2
    N_w(:,frame_no)=fft(noise_w(:,frame_no));
    noise_psd(:) = noise_psd(:) + abs(N_w(:,frame_no)).^2;
end
noise_psd = noise_psd./(N_frames_n-2); % Taking average, and adjust power gain of noise

for g=1:100
    x_hat_g = wiener(noise_psd.*(g/2), N_frames, y_w, N, x_hat, overlap);
    mse_2(g)=mean((abs(fft(x_hat_g))-abs(fft(x_in))).^2);
end

% Plots
plot(gain_list,mse_1, gain_list, mse_2)
title("Plot of MSE over white noise spectrum gain")
xlabel("Noise gain")
ylabel("MSE")
legend("var = 0.001", "var = 0.005")
ylim([0,2])

function x_hat = wiener(noise_psd, N_frames, y_w, N, x_hat, overlap)
    for frame_no=1:N_frames-2
        Y_w(:,frame_no)=fft(y_w(:,frame_no));
        X_w(:,frame_no)=Y_w(:,frame_no);
        for i=2:length(Y_w((1:N/2),frame_no))
            if abs(Y_w(i,frame_no))^2 > noise_psd(i)
                X_w(i,frame_no) = Y_w(i,frame_no)*(abs(Y_w(i,frame_no))-sqrt(noise_psd(i)))/(abs(Y_w(i,frame_no)));
            else
                X_w(i,frame_no) = 0;
            end
        end
        X_w(N:-1:N/2+2,frame_no)=conj(X_w(2:N/2,frame_no)); % Conjugate symmetry
        x_hat_w(:,frame_no)=ifft(X_w(:,frame_no));
        % Now overlap-add method
        x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)=...
        x_hat((frame_no-1)*overlap+1:(frame_no-1)*overlap+N)+x_hat_w(:,frame_no);
    end
end