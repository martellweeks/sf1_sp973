% Bayesian estimation of autoregressive model on audio file
clear

% Model definition
N = 2000;
e_var = 10^-6; % noise variance

% Read audio file and cut
[sig, Fs] = audioread('Audio_1\grosse_original.wav');
offset = 30000;
y = sig(offset:offset+N-1);
sound(y, Fs)

P_axis = 1:1:20;
ML_error_axis = zeros(20,1);
MAP_error_axis = zeros(20,1);
model_evidence_axis = zeros(20,1);

for i=1:1:20
    Pcur = P_axis(i);
    [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, Pcur, y, e_var);   
    ML_error_axis(i) = MLerror;
    MAP_error_axis(i) = MAPerror;
    model_evidence_axis(i)=model_evidence;
end

figure(4)
tiledlayout(3,1)
nexttile
plot(P_axis,ML_error_axis)
nexttile
plot(P_axis,MAP_error_axis)
nexttile
plot(P_axis, model_evidence_axis)
title("Model evidences of different AR models for music")
xlabel("AR order, P")
ylabel("Model evidence")

% Function for ML and MAP estimates with statistics
function [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, P, y, e_var)
    G = ones(N-P,P);
    for ii=1:1:N-P
        for jj=1:1:P
            G(ii,jj) = y(P+ii-jj);
        end
    end

    % Cut out initial values that do not take whole P pre-values
    y_cut = y(P+1:length(y));
    
    % ML estimate
    MLest = (G'*G)\(G'*y_cut);

    MLerror = mean((y_cut-G*MLest).^2);
    
    % Define Gaussian priors
    prior_mean = (1/P)*ones(P,1);
    prior_var = 1;
    prior_cov = sqrt(prior_var).*eye(P);
    
    % MAP estimate
    PI = G'*G+e_var*inv(prior_cov);
    THETA = G'*y_cut+e_var.*inv(prior_cov)*prior_mean;
    MAPest = PI\THETA;

    MAPerror = mean((y_cut-G*MAPest).^2);

    % Log Marginal likelihood
    model_evidence = -(P/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-P)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAPest)/(2*e_var);
end