% Pole condition and DFT of AR2 model
clear

% Model definition
P = 2;
N = 1000;
pole(1)=0.95*exp(1i*0.2*pi);
pole(3)=conj(pole(1));
pole(2)=0.9*exp(1i*0.5*pi);
pole(4)=conj(pole(2));
a=poly(pole);
e_var = 10^-6; % noise variance
e=randn(N,1)*sqrt(e_var);
sigma_e = sqrt(e_var);
x=filter(1,a,e);
figure(1)
subplot(211), plot(e)
title('e_n')
subplot(212), plot(x)
title('y_n')
[H,w]=freqz(1,a);
figure(2)
subplot(211),
semilogy(w,abs(H).^2*sigma_e^2)
title('Power spectrum of AR4 model')
xlim([0 pi])
subplot(212),
X=(abs(fft(x)).^2);
semilogy((0:N/2-1)*pi*2/N,X(1:N/2))
title('|DFT|^2 of generated data')
xlim([0 pi])