% Bayesian estimation of autoregressive model
clear

% Model definition
P = 12;
N = 10000;
theta = round((1/P)*rand(P,1),2); % theta = AR coefficients
e_var = 10^-6; % noise variance
y = zeros(N,1);
y(1) = sqrt(e_var)*randn(1);
for ii=2:1:N
    for jj=1:1:P
        if ii-jj > 0
            y(ii) = y(ii)+theta(jj)*y(ii-jj);
        end
    end
    y(ii) = y(ii) + sqrt(e_var)*randn(1);
end

P_axis = 1:1:20;
ML_error_axis = zeros(20,1);
MAP_error_axis = zeros(20,1);
model_evidence_axis = zeros(20,1);

for i=1:1:20
    Pcur = P_axis(i);
    % Model matrix for AR model
    G = ones(N-Pcur,Pcur);
    for ii=1:1:N-Pcur
        for jj=1:1:Pcur
            G(ii,jj) = y(Pcur+ii-jj);
        end
    end

    % Cut out initial values that do not take whole P pre-values
    y_cut = y(Pcur+1:length(y));
    
    % ML estimate
    MLest = (G'*G)\(G'*y_cut);

    ML_error_axis(i) = mean((y_cut-G*MLest).^2);
    
    % Define Gaussian priors
    prior_mean = (1/Pcur)*ones(Pcur,1);
    prior_var = 1;
    prior_cov = sqrt(prior_var).*eye(Pcur);
    
    % MAP estimate
    PI = G'*G+e_var*inv(prior_cov);
    THETA = G'*y_cut+e_var.*inv(prior_cov)*prior_mean;
    MAPest = PI\THETA;

    MAP_error_axis(i) = mean((y_cut-G*MAPest).^2);

    % Log Marginal likelihood
    mmll = -(Pcur/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-Pcur)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAPest)/(2*e_var);
    model_evidence_axis(i)=mmll;
end


figure(4)
tiledlayout(3,1)
nexttile
plot(P_axis,ML_error_axis)
title("Mean sum squared residual error for ML estimates by AR models")
xlabel("AR order, P")
ylabel("error")
nexttile
plot(P_axis,MAP_error_axis)
title("Mean sum squared residual error for MAP estimates by AR models")
xlabel("AR order, P")
ylabel("error")
nexttile
plot(P_axis, model_evidence_axis)
title("Model evidences of different AR order models")
xlabel("AR order, P")
ylabel("Model evidence")