% Bayesian model choice

clear

% Define model parameters
N = 1000;
P_m2 = 1;
P_m3 = 2;
theta_m2 = 5;
theta_m3 = zeros(2,1);
G_m2 = ones(N,1);
G_m3 = ones(N,2); G_m3(:,2) = 1:1:N;
theta_m3(1) = 10; theta_m3(2) = 1;
n_axis = 1:1:N;
e_var = 10^-4;
e = sqrt(e_var).*randn(N, 1);

% Create data using model 3
x = G_m3*theta_m3 + e;

% Define prior distribution
prior_mu_m2 = 0;
prior_mu_m3 = zeros(2,1);
prior_var_factor = 100000000;
prior_cov_m2 = prior_var_factor;
prior_cov_m3 = prior_var_factor*ones(2,2); prior_cov_m3(1,2) = 0; prior_cov_m3(2,1) = 0;

% Marginal likelihood using first model - no theta, no prior
% Hence MAP solution = ML solution,
% Marginal likelihood = simply pe(x)
ll_m1 = -(N/2)*log(2*pi*e_var) - x'*x




% Marginal likelihood using second model

% MAP estimate using second model
PI_m2 = G_m2'*G_m2+e_var*inv(prior_cov_m2);
THETA_m2 = G_m2'*x+e_var*inv(prior_cov_m2)*prior_mu_m2;
MAP_m2 = PI_m2\THETA_m2

% Log likelihood as a function of theta
scale = 10^-8;
tax = MAP_m2-100*scale:scale:MAP_m2+100*scale;
ll_m2 = zeros(size(tax));
for i=1:length(tax)
    ll_m2(i) = -(N/2)*log(2*pi*e_var)-(x-G_m2*tax(i))'*(x-G_m2*tax(i));
end
%figure(1); plot(tax, ll_m2)

% Posterior probability as a function of theta
post_mu_m2 = MAP_m2;
post_sigma_m2 = PI_m2\e_var;
post_pdf_m2 = normpdf(tax, post_mu_m2, post_sigma_m2);
figure(1);
plot(tax, post_pdf_m2)
title("Posterior distribution based on model 2")
xlabel("\theta")
ylabel("Probability")

% Model marginal likelihood (Bayesian)
mml_m2 = exp((-1)*(x'*x+e_var*prior_mu_m2*inv(prior_cov_m2)*prior_mu_m2-THETA_m2*MAP_m2)/(2*e_var))/...
    (((2*pi)^(P_m2/2))*sqrt(det(prior_cov_m2))*sqrt(det(PI_m2))*(2*pi*e_var)^((N-1)/2));
mmll_m2 = -0.5*(2*pi)-0.5*det(prior_cov_m2)-0.5*det(PI_m2)-((N-1)/2)*(2*pi*e_var)...
    -(x'*x+e_var*prior_mu_m2*inv(prior_cov_m2)*prior_mu_m2-THETA_m2*MAP_m2)/(2*e_var)




% MAP estimate using third model
PI_m3 = G_m3'*G_m3+e_var*inv(prior_cov_m3);
THETA_m3 = G_m3'*x+e_var*inv(prior_cov_m3)*prior_mu_m3;
MAP_m3 = PI_m3\THETA_m3

% Log likelihood as a function of theta
tax1 = 8:0.1:12;
tax2 = 0.8:0.01:1.2;
theta_axis_m3 = meshgrid(tax1, tax2);
ll_m3 = zeros(length(tax1),length(tax2));
for i=1:length(tax1)
    for j=1:length(tax2)
        ll_m3(i,j) = -(N/2)*log(2*pi*e_var)-(x-G_m3*[tax1(i);tax2(j)])'*(x-G_m3*[tax1(i);tax2(j)]);
    end
end
%figure(2); surf(tax1, tax2, ll_m3)

% Posterior probability as a function of theta
post_mu_m3 = MAP_m3;
post_cov_m3 = inv(PI_m3).*e_var;
[X1,X2] = meshgrid(tax1,tax2);
post_pdf_m3 = mvnpdf([X1(:) X2(:)], post_mu_m3', post_cov_m3);
post_pdf_m3 = reshape(post_pdf_m3, length(tax1), length(tax2));
figure(2);
surf(tax1, tax2, post_pdf_m3)
title("Posterior distribution based on model 3")
xlabel("\theta_1")
ylabel("\theta_2")
zlabel("Probability")

% Model marginal likelihood (Bayesian)
mml_m3 = exp((-1)*(x'*x+e_var.*(prior_mu_m3'*inv(prior_cov_m3)*prior_mu_m3)-THETA_m3'*MAP_m3)/(2*e_var))/...
    (((2*pi)^(P_m3/2))*sqrt(det(prior_cov_m3))*sqrt(det(PI_m3))*(2*pi*e_var)^((N-P_m3)/2));

mmll_m3 = -(P_m3/2)*(2*pi)-0.5*det(prior_cov_m3)-0.5*det(PI_m3)-((N-P_m3)/2)*(2*pi*e_var)...
    -(x'*x+e_var*(prior_mu_m3'*inv(prior_cov_m3)*prior_mu_m3)-THETA_m3'*MAP_m3)/(2*e_var)

error_m2 = mean((x-G_m2*theta_m2).^2)
error_m3 = mean((x-G_m3*theta_m3).^2)

disp(mmll_m3 > mmll_m2)