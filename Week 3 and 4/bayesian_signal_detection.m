clear
load('hidden_data.mat');

P = 1;
N = length(y);
offset = 0;
theta = 0;
signal=[-3 5 -2 4 1 3 5 -1 2 4 6 5 -2 -2 1];

% model definition
% model data = G * theta + e
e_sigma = 2;
e_var = e_sigma^2;
e = e_sigma*randn(N,1);
G = zeros(N,1);

% Define prior parameters
prior_mean = 0;
prior_var = 1;

offset_axis = 1:1:86;
data_axis = 1:1:N;
theta_map = 0.*offset_axis;
log_model_evidence = 0.*offset_axis;
PIs = 0.*offset_axis;
THETAs = 0.*offset_axis;
for i=1:1:86
    offset=offset_axis(i);
    G = zeros(N,1);
    G(offset:offset+14) = signal;

    % MAP estimate
    PI = G'*G+e_var*inv(prior_var);
    THETA = G'*y+e_var*inv(prior_var)*prior_mean;
    MAP = PI\THETA;
    theta_map(i) = MAP;
    PIs(i) = PI;
    THETAs(i) = THETA;

    % Log Marginal likelihood
    mmll = -(P/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-P)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAP)/(2*e_var);
    log_model_evidence(i) = mmll;
end

% Identify best model and corresponding theta
[best_ll, best_offset] = max(log_model_evidence);
theta_map_best = theta_map(best_offset);
G_best = zeros(N,1);
G_best(best_offset:best_offset+14) = signal;
m = G_best.*theta_map_best + e;

% Plot results
figure(3);
tiledlayout(3,1);
nexttile
plot(offset_axis,theta_map)
xlim([0 86])
title("MAP estimate of \theta for each offset model")
xlabel("offset")
ylabel("\theta")
nexttile
plot(offset_axis,log_model_evidence)
title("Model evidence for each offset model")
xlabel("offset")
ylabel("Log marginal likelihood")
xlim([0 86])
nexttile
plot(data_axis, y, data_axis, m)
title("Simulation of best offset and corresponding theta")
legend("given signal", "simulated signal")