[sig, Fs] = audioread("Audio_1\grosse_original.wav");
N = length(sig);
ratio = 0.20;
frame = 500;
for i=1:1:floor(N/frame)
    sig(i*frame:i*frame+ratio*frame-1) = zeros(ratio*frame,1);
end
idx = sig==0;
disp(sum(idx(:))/N)

audiowrite("missing_packet\grosse_20_missing.wav", sig, Fs)