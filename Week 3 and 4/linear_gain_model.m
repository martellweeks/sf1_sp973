% ML and Bayesian method of linear gain model with multiple parameters

P = 2; % Number of parameters
N = 10000; % Number of data samples
theta = zeros(P,1);
G = ones(N,P);
theta(1) = 100;
theta(2) = 0.1;
n_axis = 1:1:N;
G(:,2) = 1:1:N;
e_var = 100;
e = sqrt(e_var).*randn(N, 1);
y = G*theta + e;
plot(y)
title("Linear model with unknown noise")
xlabel("time")
ylabel("y")

% Log Likelihood function - do you need this?
%theta_axis = 2.5:0.1:3.5;
%ll = zeros(size(theta_axis));
%for i=1:length(theta_axis)
%    ll(i) = -(N/2)*log(2*pi*e_var) - transpose(y-G*theta_axis(i))*(y-G*theta_axis(i));
%end
%tiledlayout(2,1)
%nexttile
%plot(theta_axis, ll)
%title("Log likelihood function")
%xlabel("theta")
%ylabel("Log likelihood")

%ML estimate
MLest = (transpose(G)*G)\transpose(G)*y

% Prior distribution defined as Gaussian
prior_mean = theta;
prior_cov = ones(P);
prior_cov(1,2) = 0; prior_cov(2,1) = 0;

% MAP estimate
PI = transpose(G)*G+e_var*inv(prior_cov);
THETA = transpose(G)*y+e_var*inv(prior_cov)*prior_mean;
MAPest = PI\THETA

% Posterior distribution
post_mean = MAPest;
post_cov = inv(PI).*e_var
x1 = theta(1)-0.5:0.01:theta(1)+0.5;
x2 = theta(2)-0.5:0.01:theta(2)+0.5;
[X1,X2] = meshgrid(x1,x2);
X = [X1(:) X2(:)];
post_pdf = mvnpdf(X, transpose(post_mean), post_cov);
post_pdf = reshape(post_pdf, length(x1), length(x2));

figure(6)
s=surf(x1, x2, post_pdf);
title("Posterior distribution of parameters")
xlabel("\theta_1")
ylabel('\theta_2')
zlabel('Probability')