% Bayesian estimation of autoregressive model on audio file
clear

% Model definition
e_var = 10^-4; % noise variance

% Read audio files and cut region of interest
[sig, Fs] = audioread('Audio_1\grosse_original.wav');
[sig_lost_5, Fs] = audioread('missing_packet\grosse_5_missing.wav');
[sig_lost_10, Fs] = audioread('missing_packet\grosse_10_missing.wav');
[sig_lost_15, Fs] = audioread('missing_packet\grosse_15_missing.wav');
[sig_lost_20, Fs] = audioread('missing_packet\grosse_20_missing.wav');

P = 10; % Fixed for music

mse_axis = zeros(4,1);
mse_back_axis = zeros(4,1);
mse_lost_axis = zeros(4,1);
xaxis = 5:5:20;
mse_axis(1) = mean((sig-forward(sig_lost_5, P, 25, 475, e_var)).^2);
mse_axis(2) = mean((sig-forward(sig_lost_10, P, 50, 450, e_var)).^2);
mse_axis(3) = mean((sig-forward(sig_lost_15, P, 75, 425, e_var)).^2);
mse_axis(4) = mean((sig-forward(sig_lost_20, P, 100, 400, e_var)).^2);

mse_back_axis(1) = mean((sig-backward(sig_lost_5, P, 25, 475, e_var)).^2);
mse_back_axis(2) = mean((sig-backward(sig_lost_10, P, 50, 450, e_var)).^2);
mse_back_axis(3) = mean((sig-backward(sig_lost_15, P, 75, 425, e_var)).^2);
mse_back_axis(4) = mean((sig-backward(sig_lost_20, P, 100, 400, e_var)).^2);

mse_backwn_axis(1) = mean((sig-backwardwn(sig_lost_5, P, 25, 475, e_var)).^2);
mse_backwn_axis(2) = mean((sig-backwardwn(sig_lost_10, P, 50, 450, e_var)).^2);
mse_backwn_axis(3) = mean((sig-backwardwn(sig_lost_15, P, 75, 425, e_var)).^2);
mse_backwn_axis(4) = mean((sig-backwardwn(sig_lost_20, P, 100, 400, e_var)).^2);

mse_lost_axis(1) = mean((sig-sig_lost_5).^2);
mse_lost_axis(2) = mean((sig-sig_lost_10).^2);
mse_lost_axis(3) = mean((sig-sig_lost_15).^2);
mse_lost_axis(4) = mean((sig-sig_lost_20).^2);

figure(10)
plot(xaxis, mse_axis, xaxis, mse_back_axis, xaxis, mse_backwn_axis, xaxis, mse_lost_axis)
title("MSE of retrieved signal over packet loss rate")
xlabel("Packet loss rate, %")
ylabel("MSE")
legend("Forward", "Forward + Backward without e_n", "Forward + Backward with e_n", "Original lossy signal")

sound(sig_lost_10, Fs);
pause(length(sig)/Fs);
sound(forward(sig_lost_10, P, 50, 450, e_var), Fs);
pause(length(sig)/Fs);
sound(backward(sig_lost_10, P, 50, 450, e_var), Fs);

audiowrite("missing_packet\grosse_5_forward.wav", forward(sig_lost_5, P, 25, 475, e_var), Fs)
audiowrite("missing_packet\grosse_10_forward.wav", forward(sig_lost_10, P, 50, 450, e_var), Fs)
audiowrite("missing_packet\grosse_15_forward.wav", forward(sig_lost_15, P, 75, 425, e_var), Fs)
audiowrite("missing_packet\grosse_20_forward.wav", forward(sig_lost_20, P, 100, 400, e_var), Fs)
audiowrite("missing_packet\grosse_5_both.wav", backward(sig_lost_5, P, 25, 475, e_var), Fs)
audiowrite("missing_packet\grosse_10_both.wav", backward(sig_lost_10, P, 50, 450, e_var), Fs)
audiowrite("missing_packet\grosse_15_both.wav", backward(sig_lost_15, P, 75, 425, e_var), Fs)
audiowrite("missing_packet\grosse_20_both.wav", backward(sig_lost_20, P, 100, 400, e_var), Fs)


% Function for forward prediction retrieval
function sig_retrieved = forward(sig_lost, P, count_gap, count_fill, e_var)
    N = count_fill;
    sig_retrieved = repmat(sig_lost,1);
    sig_ret_for = repmat(sig_lost,1);
    MAP_opt = zeros(P,1);
    noise_var_opt = zeros(1);
    cur = 0;
    for i=1:length(sig_retrieved)
        if sig_lost(i)==0 && sig_lost(i+1)==0
            if cur == 0
                [~, ~, MAP_opt, noise_var_opt, ~] = estimate(N, P, sig_lost(i-count_fill:i-1), e_var);
            end
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + MAP_opt(jj)*sig_ret_for(i-jj);
                sig_ret_for(i) = sig_ret_for(i) + MAP_opt(jj)*sig_ret_for(i-jj);
            end
            randnoise = noise_var_opt*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_for(i) = sig_ret_for(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end
end

% Function for forward and backward prediction retrieval
function sig_retrieved = backward(sig_lost, P, count_gap, count_fill, e_var)
    N = count_fill;
    sig_retrieved = repmat(sig_lost,1);
    sig_ret_for = repmat(sig_lost,1);
    sig_ret_back = repmat(sig_lost,1);
    MAP_opt = zeros(P,1);
    noise_var_opt = zeros(1);
    cur = 0;
    for i=1:length(sig_retrieved)
        if sig_lost(i)==0 && sig_lost(i+1)==0
            if cur == 0
                [~, ~, MAP_opt, noise_var_opt, ~] = estimate(N, P, sig_lost(i-count_fill:i-1), e_var);
            end
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
                sig_ret_for(i) = sig_ret_for(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
            end
            %randnoise = ((count_gap-cur)/count_gap)*noise_var_opt*randn(1);
            %sig_retrieved(i) = sig_retrieved(i) + randnoise;
            %sig_ret_for(i) = sig_ret_for(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end

    for i=length(sig_retrieved):-1:1
        if sig_lost(i)==0
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
                sig_ret_back(i) = sig_ret_back(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
            end
            %randnoise = ((count_gap-cur)/count_gap)*sqrt(noise_var_opt)*randn(1);
            %sig_retrieved(i) = sig_retrieved(i) + randnoise;
            %sig_ret_back(i) = sig_ret_back(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end
end

% Function for forward and backward prediction retrieval
function sig_retrieved = backwardwn(sig_lost, P, count_gap, count_fill, e_var)
    N = count_fill;
    sig_retrieved = repmat(sig_lost,1);
    sig_ret_for = repmat(sig_lost,1);
    sig_ret_back = repmat(sig_lost,1);
    MAP_opt = zeros(P,1);
    noise_var_opt = zeros(1);
    cur = 0;
    for i=1:length(sig_retrieved)
        if sig_lost(i)==0 && sig_lost(i+1)==0
            if cur == 0
                [~, ~, MAP_opt, noise_var_opt, ~] = estimate(N, P, sig_lost(i-count_fill:i-1), e_var);
            end
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
                sig_ret_for(i) = sig_ret_for(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
            end
            randnoise = ((count_gap-cur)/count_gap)*noise_var_opt*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_for(i) = sig_ret_for(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end

    for i=length(sig_retrieved):-1:1
        if sig_lost(i)==0
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
                sig_ret_back(i) = sig_ret_back(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
            end
            randnoise = ((count_gap-cur)/count_gap)*sqrt(noise_var_opt)*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_back(i) = sig_ret_back(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end
end


% Function for ML and MAP estimates with statistics
function [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, P, y, e_var)
    G = ones(N-P,P);
    for ii=1:1:N-P
        for jj=1:1:P
            G(ii,jj) = y(P+ii-jj);
        end
    end

    % Cut out initial values that do not take whole P pre-values
    y_cut = y(P+1:length(y));
    
    % ML estimate
    MLest = (G'*G)\(G'*y_cut);

    MLerror = mean((y_cut-G*MLest).^2);
    
    % Define Gaussian priors
    prior_mean = (1/P)*ones(P,1);
    prior_var = 10^-2;
    prior_cov = sqrt(prior_var).*eye(P);
    
    % MAP estimate
    PI = G'*G+e_var*inv(prior_cov);
    THETA = G'*y_cut+e_var.*inv(prior_cov)*prior_mean;
    MAPest = PI\THETA;

    MAPerror = mean((y_cut-G*MAPest).^2);

    % Log Marginal likelihood
    model_evidence = -(P/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-P)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAPest)/(2*e_var);
end