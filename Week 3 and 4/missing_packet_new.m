% Bayesian estimation of autoregressive model on audio file
clear

% Model definition
e_var = 10^-4; % noise variance

% Read audio files and cut region of interest
[sig, Fs] = audioread('Audio_1\grosse_original.wav');
[sig_lost, Fs] = audioread('missing_packet\grosse_10_missing.wav');
sound(sig_lost, Fs)
P = 4; % Fixed for music

% Packet retrieval with AR
gap = false;
count_gap = 0;
i_stop = 0;
for i=1:length(sig_retrieved)
    if sig_retrieved(i)==0
        count_gap = count_gap + 1;
        gap = true;
    end
    if sig_retrieved(i)~=0 && gap == true
        gap = false;
        i_stop = i;
        break
    end
end
fill = false;
count_fill = 0;
for i=i_stop+1:length(sig_retrieved)
    if sig_retrieved(i)~=0
        count_fill = count_fill + 1;
        fill = true;
    end
    if sig_retrieved(i)==0 && fill == true
        fill = false;
        break
    end
end



figure(5)
tiledlayout(2,1)
nexttile
plot(sig_lost)
title("Signal with lost packets")
xlim([62500 65500])
nexttile
plot(sig_retrieved)
title("Recovered signal, with forward prediction")
xlim([62500 65500])

pause(length(sig_lost)/Fs)
sound(sig_retrieved, Fs)

mse_before = mean((sig-sig_lost).^2)
mse_after = mean((sig-sig_retrieved).^2)

% Function for forward prediction retrieval

function [sig_retrieved] = forward(sig_lost, P, count_gap, count_fill)
    N = count_fill;
    sig_retrieved = repmat(sig_lost,1);
    sig_ret_for = repmat(sig_lost,1);
    MAP_opt = zeros(P,1);
    noise_var_opt = zeros(1);
    cur = 0;
    for i=1:length(sig_retrieved)
        if sig_lost(i)==0 && sig_lost(i+1)==0
            if cur == 0
                [~, ~, MAP_opt, noise_var_opt, ~] = estimate(N, P, sig_lost(i-count_fill:i-1), e_var);
            end
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
                sig_ret_for(i) = sig_ret_for(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
            end
            randnoise = ((count_gap-cur)/count_gap)*noise_var_opt*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_for(i) = sig_ret_for(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end
end

function [sig_retrieved] = backward(sig_lost, P, count_gap, count_fill)
    N = count_fill;
    sig_retrieved = repmat(sig_lost,1);
    sig_ret_for = repmat(sig_lost,1);
    sig_ret_back = repmat(sig_lost,1);
    MAP_opt = zeros(P,1);
    noise_var_opt = zeros(1);
    cur = 0;
    for i=1:length(sig_retrieved)
        if sig_lost(i)==0 && sig_lost(i+1)==0
            if cur == 0
                [~, ~, MAP_opt, noise_var_opt, ~] = estimate(N, P, sig_lost(i-count_fill:i-1), e_var);
            end
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
                sig_ret_for(i) = sig_ret_for(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_for(i-jj);
            end
            randnoise = ((count_gap-cur)/count_gap)*noise_var_opt*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_for(i) = sig_ret_for(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end

    for i=length(sig_retrieved):-1:1
        if sig_lost(i)==0
            cur = cur + 1;
            for jj = 1:P
                sig_retrieved(i) = sig_retrieved(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
                sig_ret_back(i) = sig_ret_back(i) + ((count_gap-cur)/count_gap)*MAP_opt(jj)*sig_ret_back(i+jj);
            end
            randnoise = ((count_gap-cur)/count_gap)*sqrt(noise_var_opt)*randn(1);
            sig_retrieved(i) = sig_retrieved(i) + randnoise;
            sig_ret_back(i) = sig_ret_back(i) + randnoise;
            if cur == count_gap
                cur = 0;
            end
        end
    end
end

% Function for ML and MAP estimates with statistics
function [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, P, y, e_var)
    G = ones(N-P,P);
    for ii=1:1:N-P
        for jj=1:1:P
            G(ii,jj) = y(P+ii-jj);
        end
    end

    % Cut out initial values that do not take whole P pre-values
    y_cut = y(P+1:length(y));
    
    % ML estimate
    MLest = (G'*G)\(G'*y_cut);

    MLerror = mean((y_cut-G*MLest).^2);
    
    % Define Gaussian priors
    prior_mean = (1/P)*ones(P,1);
    prior_var = 10^-2;
    prior_cov = sqrt(prior_var).*eye(P);
    
    % MAP estimate
    PI = G'*G+e_var*inv(prior_cov);
    THETA = G'*y_cut+e_var.*inv(prior_cov)*prior_mean;
    MAPest = PI\THETA;

    MAPerror = mean((y_cut-G*MAPest).^2);

    % Log Marginal likelihood
    model_evidence = -(P/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-P)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAPest)/(2*e_var);
end