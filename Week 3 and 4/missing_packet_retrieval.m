% Bayesian estimation of autoregressive model on audio file
clear

% Model definition
N = 1000;
e_var = 10^-3; % noise variance

% Read audio files and cut region of interest
[sig, Fs] = audioread('Audio_1\armst_37_orig.wav');
[sig_lost, Fs] = audioread('missing_packet\armst_20_missing.wav');
offset = 1001;
y = sig(offset:offset+N-1);

P_axis = 1:1:20;
ML_error_axis = zeros(20,1);
MAP_error_axis = zeros(20,1);
model_evidence_axis = zeros(20,1);

for i=1:1:20
    Pcur = P_axis(i);
    [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, Pcur, y, e_var);   
    ML_error_axis(i) = MLerror;
    MAP_error_axis(i) = MAPerror;
    model_evidence_axis(i)=model_evidence;
end

figure(4)
tiledlayout(3,1)
nexttile
plot(P_axis,ML_error_axis)
nexttile
plot(P_axis,MAP_error_axis)
nexttile
plot(P_axis, model_evidence_axis)

[~, P_opt] = max(model_evidence_axis);
[ML_opt, ~, MAP_opt, MAPerror_opt, ~] = estimate(N, P_opt, y, e_var);
disp(MAP_opt)
noise_var_opt = MAPerror_opt;

sound(sig_lost, Fs)

% Packet retrieval with AR
sig_retrieved = repmat(sig_lost,1);
gap = false;
count = 0;
for i=1:length(sig_retrieved)
    if sig_retrieved(i)==0
        count = count + 1;
        gap = true;
    end
    if sig_retrieved(i)~=0 && gap == true
        gap = false;
        break
    end
end

cur = 0;
for i=1:length(sig_retrieved)
    if sig_lost(i)==0
        cur = cur + 1;
        for jj = 1:P_opt
            sig_retrieved(i) = sig_retrieved(i) + MAP_opt(jj)*sig_retrieved(i-jj);
        end
        sig_retrieved(i) = sig_retrieved(i) + noise_var_opt*randn(1);
        if cur == count
            cur = 0;
        end
    end
end

%for i=length(sig_retrieved):-1:1
%    if sig_lost(i)==0
%        cur = cur + 1;
%        for jj = 1:P_opt
%            sig_retrieved(i) = sig_retrieved(i) + ((count-cur)/count)*MAP_opt(jj)*sig_retrieved(i+jj);
%        end
%        sig_retrieved(i) = sig_retrieved(i) + ((count-cur)/count)*sqrt(noise_var_opt)*randn(1);
%        if cur == count
%            cur = 0;
%        end
%    end
%end

figure(5)
tiledlayout(2,1)
nexttile
plot(sig_lost)
nexttile
plot(sig_retrieved)

pause(length(sig_lost)/Fs)
sound(sig_retrieved, Fs)


% Function for ML and MAP estimates with statistics
function [MLest, MLerror, MAPest, MAPerror, model_evidence] = estimate(N, P, y, e_var)
    G = ones(N-P,P);
    for ii=1:1:N-P
        for jj=1:1:P
            G(ii,jj) = y(P+ii-jj);
        end
    end

    % Cut out initial values that do not take whole P pre-values
    y_cut = y(P+1:length(y));
    
    % ML estimate
    MLest = (G'*G)\(G'*y_cut);

    MLerror = mean((y_cut-G*MLest).^2);
    
    % Define Gaussian priors
    prior_mean = (1/P)*ones(P,1);
    prior_var = 10^-2;
    prior_cov = sqrt(prior_var).*eye(P);
    
    % MAP estimate
    PI = G'*G+e_var*inv(prior_cov);
    THETA = G'*y_cut+e_var.*inv(prior_cov)*prior_mean;
    MAPest = PI\THETA;

    MAPerror = mean((y_cut-G*MAPest).^2);

    % Log Marginal likelihood
    model_evidence = -(P/2)*(2*pi)-0.5*det(prior_var)-0.5*det(PI)-((N-P)/2)*(2*pi*e_var)...
    -(y'*y+e_var*(prior_mean'*inv(prior_var)*prior_mean)-THETA'*MAPest)/(2*e_var);
end