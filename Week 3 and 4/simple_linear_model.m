% ML and Bayesian method of simple linear model

theta = 3; % Constant parameter
N = 100;
e_var = 1;
e = sqrt(e_var).*randn(N, 1);
y = e+theta;
G = ones(N,1);

% Log Likelihood function
theta_axis = 2.9:0.001:3.1;
ll = zeros(size(theta_axis));
for i=1:length(theta_axis)
    ll(i) = -(N/2)*log(2*pi*e_var) - (y-G*theta_axis(i))'*(y-G*theta_axis(i));
end
tiledlayout(2,1)
nexttile
plot(theta_axis, ll)
title("Log likelihood function")
xlabel("theta")
ylabel("Log likelihood")
xlim([2.9 3.1])

%ML estimate
MLest = (G'*G)\(G'*y)

% Prior distribution defined as Gaussian
prior_mean = theta;
prior_cov = 0.01;

% MAP estimate
PI = G'*G+e_var*inv(prior_cov);
THETA = G'*y+e_var*inv(prior_cov)*prior_mean;
MAPest = PI\THETA

% Posterior distribution
post_mean = MAPest;
post_cov = PI\e_var;
pdfaxis = [2.9:0.0001:3.1];
post_pdf1 = normpdf(pdfaxis, post_mean, post_cov);
[maxval, ~] = max(post_pdf1); 
post_pdf1 = post_pdf1./maxval;

% Prior distribution defined as Gaussian
prior_mean = theta;
prior_cov = 1;

% MAP estimate
PI = G'*G+e_var*inv(prior_cov);
THETA = G'*y+e_var*inv(prior_cov)*prior_mean;
MAPest = PI\THETA

% Posterior distribution
post_mean = MAPest;
post_cov = PI\e_var;
pdfaxis = [2.9:0.0001:3.1];
post_pdf2 = normpdf(pdfaxis, post_mean, post_cov);
[maxval, ~] = max(post_pdf2); 
post_pdf2 = post_pdf2./maxval

% Prior distribution defined as Gaussian
prior_mean = theta;
prior_cov = 10000;

% MAP estimate
PI = G'*G+e_var*inv(prior_cov);
THETA = G'*y+e_var*inv(prior_cov)*prior_mean;
MAPest = PI\THETA

% Posterior distribution
post_mean = MAPest;
post_cov = PI\e_var;
pdfaxis = [2.9:0.0001:3.1];
post_pdf3 = normpdf(pdfaxis, post_mean, post_cov);
[maxval, ~] = max(post_pdf3); 
post_pdf3 = post_pdf3./maxval;


nexttile
plot(pdfaxis, post_pdf1, pdfaxis, post_pdf2, pdfaxis, post_pdf3)
title("Posterior probability distribution")
xlabel("theta")
ylabel("Probability, normalised")
legend("\sigma_{\theta}^2=0.01","\sigma_{\theta}^2=1","\sigma_{\theta}^2=10000")
xlim([2.9 3.1])